package com.experitest.auto;

import java.net.MalformedURLException;
import java.net.URL;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class AndroidDemoTest extends BaseTest {
	protected static  AndroidDriver<AndroidElement> driver = null;

	@BeforeMethod
	@Parameters("deviceQuery")
	public  void setUp(@Optional("@os='android'") String deviceQuery) throws Exception {
		init(deviceQuery);
		dc.setCapability(MobileCapabilityType.APP, "cloud:net.nadsoft.zoolcellular/.ui.splash.SplashActivity");
		dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "net.nadsoft.zoolcellular");
		dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".ui.splash.SplashActivity");
		dc.setCapability("deviceName", "Galaxy A10");
		dc.setCapability("app", "C:/Users/NadSoft/Desktop/App/ZoolVersion.apk");
		dc.setCapability("testName", "AndroidDemoTest");
		dc.setCapability("deviceQuery", "@serialnumber='RF8M62VGX1M'");
		driver = new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
		

	}

	@Test
	public void test() {
		// Enter the test code

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
