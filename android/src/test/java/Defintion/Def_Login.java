package Defintion;


import java.net.MalformedURLException;

import com.experitest.auto.AndroidDemoTest;
import com.experitest.auto.BaseTest;

import MobileAutomation.android.Action_Login;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class Def_Login extends AndroidDemoTest {
	@Given("Iam in Zool Splash screen")
	public void iam_in_Zool_Splash_screen() throws MalformedURLException, InterruptedException {
	 BaseTest.setup();
	}

	@When("Login")
	public void login() throws InterruptedException {
	  Action_Login.Valid_Login();
	}

	@When("Close the App")
	public void close_the_App() {
		driver.quit();
	}

}
