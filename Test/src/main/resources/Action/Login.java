package Action;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import ZoolSite.ZoolSite.BaseTest;

public class Login extends BaseTest {

	
	public static void LoginUsingValidEmail () throws FindFailed, InterruptedException  {
		driver.findElement(By.id("login_btn")).click();
		Pattern email = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/email.PNG");
		screen.type(email, "faten@nadsoft.net");
		Pattern Password = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/password.PNG");
		screen.type(Password, "faten55132");
		Pattern Login = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/login_btn.PNG");
		screen.click(Login);
		Thread.sleep(2000);
		
	}
	public static void LoginUsingInvalidEmail() throws InterruptedException, FindFailed {
	
		driver.findElement(By.id("login_btn")).click();
		Pattern email = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/email.PNG");
		screen.type(email, "faten@nadsoft.co");
		Pattern Password = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/password.PNG");
		screen.type(Password, "faten55132");
		Pattern Login = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/login_btn.PNG");
		screen.click(Login);
		Thread.sleep(2000);
	String Message = 	driver.getPageSource();
	if (Message.contains("These credentials do not match our records."))
	{System.out.println("These credentials do not match our records");}
	else 
	{
		System.out.println("The Login has been done successfully");
	}
	
	}
	public static void LoginViaFB () throws InterruptedException, FindFailed {
		Thread.sleep(2500);
		driver.findElement(By.id("login_btn")).click();
		
		Pattern FB = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/FB.PNG");
		//screen.selectRegion();
		screen.capture(screen);
		screen.mouseMove(FB);
		screen.click(FB);
		Thread.sleep(3000);
        driver.findElement(By.id("email")).sendKeys("testnadsoft@gmail.com");
        driver.findElement(By.id("pass")).sendKeys("faten55132");
        Thread.sleep(2000);
        driver.findElement(By.id("loginbutton")).click();
		System.out.println("Register Via FaceBook has been done successfully");
	}
	public static void PressLoginButton() throws InterruptedException, FindFailed {
		driver.findElement(By.id("login_btn")).click();
		Thread.sleep(2000);
		Pattern Login = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/login_btn.PNG");
		screen.click(Login);
		Thread.sleep(3500);
	}
	public static void close_login_dialogue_box () throws InterruptedException {
		driver.findElement(By.id("login_btn")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"myModallogin\"]/div/div/div[1]/button")).click();
	    Thread.sleep(3000);
	}
}
