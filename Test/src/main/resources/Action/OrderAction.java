package Action;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import ZoolSite.ZoolSite.BaseTest;

public class OrderAction extends BaseTest {

	public static void Order_COD () throws InterruptedException, FindFailed {
		OrderAction.GoToMyCart();
		OrderAction.checkout_Page();
		
	}
	
	public static void checkout_Page () throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tfoot/tr[2]/td[3]/a")).click();
		driver.findElement(By.name("quest_name")).sendKeys("Faten");
		driver.findElement(By.name("quest_phone")).sendKeys("0000000000");
		driver.findElement(By.name("payment_address")).sendKeys("City xc , Street YY Patudi Building");
		driver.findElement(By.name("shipping_address")).sendKeys("City xc , Street YY Patudi Building");
		driver.findElement(By.xpath("//*[@id=\"formcontact\"]/form/input[7]")).click();
		Thread.sleep(2000);
		String title = driver.getPageSource();
		if (title.contains("Your Order Success send, Thank you"))
		{System.out.println("Congrats place order has been done successfully");}
		else {System.out.println("Place order has been failed ");}
	}
	
	
	public static void GoToMyCart () throws FindFailed {
		Pattern cart = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/cart.PNG");
		screen.mouseMove(cart);
		screen.click(cart);
		driver.findElement(By.xpath("//*[@id=\"menu\"]/div[1]/div/div/div/div[3]/div/a")).click();
	}
}
