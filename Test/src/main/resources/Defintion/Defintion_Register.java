package Defintion;

import Action.Registration_Action;
import ZoolSite.ZoolSite.BaseTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Defintion_Register extends BaseTest {

	@Given("^Iam in zool site$")
	public void iam_in_zool_site() throws Throwable {
		BaseTest.openbrowser();
		BaseTest.navigateTo(Zoolurl);
	}

	@When("^Register using vaild email$")
	public void register_using_vaild_email() throws Throwable {
	    Registration_Action.Valid_Register();
	}
	@Then("^close the browser$")
	public void close_the_browser() throws Throwable {
	  Registration_Action.close();
	 
	}

	@When("^Register VIA Email$")
	public void register_VIA_Email() throws Throwable {
	    Registration_Action.Register_VIA_FB();
	}
	@When("^Register using Invaild email$")
	public void register_using_Invaild_email() throws Throwable {
	   Registration_Action.InValid_Register();
	}

	@When("^Logout$")
	public void logout() throws Throwable {
		Registration_Action.Logout();
	  
	}


}
