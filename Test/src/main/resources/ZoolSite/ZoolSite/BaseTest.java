package ZoolSite.ZoolSite;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.Screen;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class BaseTest {
	public static WebDriver driver;
	public static String Zoolurl = "https://zoolc.co.il";
	static Random rand = new Random();
   public static int rnd = rand.nextInt(999) +1 ;
   protected static Screen screen = new Screen();
    
 

	public static void openbrowser() {
		System.setProperty("webdriver.chrome.driver", "./Browser/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
	}

	public static void navigateTo(String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("navigate to : " + driver.getCurrentUrl());
	    System.out.println(driver.getTitle());
	}

	

	

}


