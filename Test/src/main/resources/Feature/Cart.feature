#Author: faten@nadsoft.net
@Cart
Feature: Shopping Cart

  @AddProduct 
  Scenario: Add Product To Cart
    Given Iam in zool site
    When Add Product To My Cart
    Then close the browser

  @AddMultipleProduct
  Scenario: Add Multiple Product
    Given Iam in zool site
    When Add Multiple Product To My Cart
    Then close the browser

    @DeleteProduct
    Scenario: Delete Product from My cart
    Given Iam in zool site
    When Delete Product
    Then close the browser
    
    @UpdateQuantity
    Scenario: Update The Quantity
    Given Iam in zool site
    When Update Quantity on My Cart
    Then close the browser