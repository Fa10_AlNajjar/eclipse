#Author: faten@nadsoft.net
#This Login zool scenarios
@Login
Feature: Login

  @closeLoginDialougeBox
  Scenario: Close Login Dialouge
    Given Iam in zool site
    When close login dialouge
    Then close the browser

  @HappyLogin
  Scenario: Happy login
    Given Iam in zool site
    When Login using vaild email
    Then close the browser

  @UnhappyScenario
  Scenario: Unhappy Login
    Given Iam in zool site
    When Login using In-vaild email
    Then close the browser

  @LoginViaFaceBook
  Scenario: Login Via Facebook
    Given Iam in zool site
    When Login Via FB
    Then close the browser

  @LoginWithoutFillData
  Scenario: LoginWithoutFillData
    Given Iam in zool site
    When Press login button
    Then close the browser
