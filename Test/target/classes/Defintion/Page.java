package Defintion;

import Action.Action_Page;
import cucumber.api.java.en.When;

public class Page {
	@When("^Iam in Main Page$")
	public void iam_in_Main_Page() throws Throwable {
	 Action_Page.HomePage(); 
	}

	@When("^Iam in About-us Page$")
	public void iam_in_About_us_Page() throws Throwable {
	    Action_Page.AboutUs();
	}

	@When("^Iam in NewProduct Page$")
	public void iam_in_NewProduct_Page() throws Throwable {
	 Action_Page.New_Product_Page();  
	}

	@When("^Iam in Package-List Page$")
	public void iam_in_Package_List_Page() throws Throwable {
	    Action_Page.Package_Page();
	}

	@When("^Iam in Used-Product Page$")
	public void iam_in_Used_Product_Page() throws Throwable {
	  Action_Page.Used_Product_Page();
	}

	@When("^Iam in Contact-us Page$")
	public void iam_in_Contact_us_Page() throws Throwable {
	    Action_Page.contact_us_Page();
	}
}
