package Defintion;

import Action.Login;
import cucumber.api.java.en.When;

public class Login_Def {
	@When("^Login using vaild email$")
	public void login_using_vaild_email() throws Throwable {
		Login.LoginUsingValidEmail();
	}

	@When("^Login using In-vaild email$")
	public void login_using_In_vaild_email() throws Throwable {
	Login.LoginUsingInvalidEmail();
	}

	@When("^Login Via FB$")
	public void login_Via_FB() throws Throwable {
		Login.LoginViaFB();
	}
	@When("^Press login button$")
	public void press_login_button() throws Throwable {
	   Login.PressLoginButton();
	}
	@When("^close login dialouge$")
	public void close_login_dialouge() throws Throwable {
	  Login.close_login_dialogue_box();
	}


}
