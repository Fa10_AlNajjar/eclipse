package Action;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import ZoolSite.ZoolSite.BaseTest;

public class Registration_Action extends BaseTest {

	public static void Valid_Register() throws InterruptedException {
		Thread.sleep(2500);
		driver.findElement(By.id("register_btn")).click();
		driver.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("Faten");
		// driver.findElement(By.id("name")).sendKeys("Faten");
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("faten+" + rnd + "@nadsoft.net");
		// driver.findElement(By.id("email")).sendKeys("faten+"+rnd+"@nadsoft.net");
		System.out.println("faten+" + rnd + "@nadsoft.net");
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("faten55132");
		// driver.findElement(By.id("password")).sendKeys("faten55132");
		driver.findElement(By.xpath("//*[@id=\"password-confirm\"]")).sendKeys("faten55132");
		// driver.findElement(By.id("password-confirm")).sendKeys("faten55132");
		driver.findElement(By.id("submit")).click();
		Thread.sleep(3000);
		System.out.println("Register has been done successfully");

	}

	public static void InValid_Register() throws InterruptedException {
		Thread.sleep(2500);
		driver.findElement(By.id("register_btn")).click();
		driver.findElement(By.xpath("//*[@id=\"name\"]")).sendKeys("Faten");
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("faten@nadsoft.net");
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("faten55132");
		driver.findElement(By.xpath("//*[@id=\"password-confirm\"]")).sendKeys("faten55132");
		driver.findElement(By.id("submit")).click();
		Thread.sleep(3000);
		String x = "The email has already been taken.";
		if (driver.getPageSource().contains(x)) {
			System.out.println("The email has already been taken.");
		} else {
			System.out.println("Register using invalid Email has been failed");
		}
	}

	public static void Register_VIA_FB() throws InterruptedException, FindFailed {
		Thread.sleep(2500);
		driver.findElement(By.id("register_btn")).click();
		/* driver.findElement(By.xpath("//*[@id=\"myModalreg\"]/div/div/div[2]/a/img")).click();*/
		
		Pattern FB = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/FB.PNG");
		// screen.selectRegion();
		screen.capture(screen);
		screen.mouseMove(FB);
		screen.click(FB);
		Thread.sleep(3000);
		driver.findElement(By.id("email")).sendKeys("testnadsoft@gmail.com");
		driver.findElement(By.id("pass")).sendKeys("faten55132");
		Thread.sleep(2000);
		driver.findElement(By.id("loginbutton")).click();
		System.out.println("Register Via FaceBook has been done successfully");

	}

	public static void Logout() throws InterruptedException, FindFailed {
		Registration_Action.Register_VIA_FB();
		Thread.sleep(2500);
		driver.findElement(By.xpath("//*[@id=\"top\"]/div[3]/div/a[2]")).click();
		System.out.println("Logout Has Been Done Successfully");

	}

	public static void close() {
		driver.close();
	  

	}
}
