package Action;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import ZoolSite.ZoolSite.BaseTest;

public class Cart_Action extends BaseTest {
	public static void Add_Single_Product() throws InterruptedException {
		Search_Action.Search_On_Product();
		driver.findElement(By.xpath("//*[@id=\"content\"]/div[4]/div/div/p[2]/a")).click();
		String title = driver.getPageSource();
		if (title.contains("Product added to cart successfully!")) {
			System.out.println("Add Single Product Has Been Done Succesfully");
		} else {
			System.out.println("Add Single Product Has Been Failed");
		}
	}

	public static void Add_Multiple_Product() throws InterruptedException {
		Search_Action.Search_On_Product();
		driver.findElement(By.xpath("//*[@id=\"content\"]/div[4]/div/div/p[2]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div/div/p[2]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div/div/p[2]/a")).click();
		String title = driver.getPageSource();
		if (title.contains("Product added to cart successfully!")) {
			System.out.println("Add Multiple Products Have Been Done Succesfully");
		} else {
			System.out.println("Add Multiple Products Have Been Failed");
		}
	}

	public static void Delet_from_Cart() throws InterruptedException, FindFailed {
		Cart_Action.Add_Multiple_Product();
		Pattern cart = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/cart.PNG");
		screen.mouseMove(cart);
		screen.click(cart);
		driver.findElement(By.xpath("//*[@id=\"menu\"]/div[1]/div/div/div/div[5]/div/a")).click();
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tbody/tr[2]/td[2]/button[2]")).click();
		Pattern ok = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/ok.PNG");
		Thread.sleep(1500);
		screen.mouseMove(ok);
		screen.click(ok);
		Thread.sleep(2000);
		String title = driver.getPageSource();
		if (title.contains("Product removed successfully")) {
			System.out.println("Product Has Been Removed Successfully");
		} else {
			System.out.println("Product Removed Has Been Failed");
		}
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tbody/tr[1]/td[2]/button[2]")).click();
		Pattern cancel = new Pattern("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/cancel.PNG");
		Thread.sleep(1500);
		screen.mouseMove(cancel);
		screen.click(cancel);

	}

	public static void update_quantity() throws InterruptedException, FindFailed {
		Cart_Action.Add_Single_Product();
		Pattern cart = new Pattern ("C:/Users/NadSoft/eclipse-workspace/ZoolSite/ScreenShoot/cart.PNG");
		screen.mouseMove(cart);
		screen.click(cart);
		driver.findElement(By.xpath("//*[@id=\"menu\"]/div[1]/div/div/div/div[3]/div/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tbody/tr[1]/td[2]/input")).sendKeys("2");
		driver.findElement(By.xpath("//*[@id=\"cart\"]/tbody/tr[1]/td[2]/button[1]")).click();
		Thread.sleep(2000);
		String title = driver.getPageSource();
		if (title.contains("Cart updated successfully")) {
			System.out.println("Cart Has Been Updated Sucessfully");
		} else {
			System.out.println("Cart Update Has Been Failed");
		}
	}
}
