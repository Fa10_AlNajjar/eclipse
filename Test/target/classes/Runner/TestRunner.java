package Runner;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import cucumber.api.CucumberOptions;
import cucumber.api.java.lv.Ja;
import cucumber.runtime.model.CucumberScenario;
@RunWith (JUnit4.class)


@CucumberOptions (
		features = "Feature" ,
		glue = {"Defintions"},
		plugin ={"html:target/ cucumber-report"} )
public class TestRunner {

}
