package Runner;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import cucumber.api.CucumberOptions;
@RunWith (JUnit4.class)

@CucumberOptions (
		features = "Feature" ,
		glue = {"Defintions"},
		plugin ={"html:target/ cucumber-report"} )
public class TestRunner2 {

}
