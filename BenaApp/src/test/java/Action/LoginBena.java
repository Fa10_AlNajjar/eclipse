package Action;


import org.openqa.selenium.By;
import com.experitest.auto.BaseTest;
import Locators.Login_Bena_Locators;
import io.appium.java_client.MobileElement;

public class LoginBena extends BaseTest {

	public static void Login () throws InterruptedException {
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("faten+android1@nadsoft.net");
		driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten55132");
		driver.findElement(Login_Bena_Locators.Login).click();
		Thread.sleep(5000);
     	MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
     	if (x.isDisplayed()) {
     		System.out.println("Login by Email done successfully");
     	}
     	else {System.out.println("Login by Email failed");
     	}
	    	
	}
	
	public static void Login_by_phone() throws InterruptedException {
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("522222222");
		driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten55132");
		driver.findElement(Login_Bena_Locators.Login).click();
		Thread.sleep(5000);
		MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
     	if (x.isDisplayed()) {
     		System.out.println("Login by Email done successfully");
     	}
     	else {System.out.println("Login by Email failed");
     	}
	}
	public static void Login_By_InvalidEmail () {
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("abcd@nadsoft.net");
		driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten55132");
		driver.findElement(Login_Bena_Locators.Login).click();
		if (driver.findElement(By.id("net.nadsoft.bena:id/titleLabel")).isEnabled()) {
			System.out.println("Login By Invalid Email is passed ");
		} else {System.out.println("Login By Invalid Email is failed ");}
	}
	public static void Login_By_WrongPassword () {
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("faten+android1@nadsoft.net");
		driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten+55132");
		driver.findElement(Login_Bena_Locators.Login).click();
		if (driver.findElement(By.id("net.nadsoft.bena:id/titleLabel")).isEnabled()) {
			System.out.println("Login By wrong password is passed ");
		} else {System.out.println("Login By wrong password is failed ");}
	}
	
	public static void Skip () throws InterruptedException {
		driver.findElement(Login_Bena_Locators.Skip).click();
		MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
     	if (x.isDisplayed()) {
     		System.out.println("Skip Done successfully");
     	}
     	else {System.out.println("Skip has not been done successfully");
     	}
     	driver.findElement(Login_Bena_Locators.Allow).click();
     	Thread.sleep(1000);
     	driver.findElement(Login_Bena_Locators.Menu).click();
	}
	public static void GoTo_SignUp_From_Login () {
		driver.findElement(Login_Bena_Locators.Register_from_Login).click();
		MobileElement x = (MobileElement) driver.findElement(By.id("net.nadsoft.bena:id/titleLabel"));
     	if (x.isDisplayed()) {
     		System.out.println("Open Signup screen from Login has been done successfully");
     	}
     	else {System.out.println("Open Signup screen from Login has not been done successfully");
     	}
	}
	public static void forget_password () {
		driver.findElement(Login_Bena_Locators.Forget_Password).click();
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("522222222");
		driver.findElement(Login_Bena_Locators.Send_Verify_Number_Password).click();
		driver.findElement(By.id("net.nadsoft.bena:id/optView")).sendKeys("12345");
	}
	public static void resend_verify_number() {
		driver.findElement(Login_Bena_Locators.Forget_Password).click();
		driver.findElement(Login_Bena_Locators.Login_user_name).sendKeys("faten+android1@nadsoft.net");
		driver.findElement(Login_Bena_Locators.Send_Verify_Number_Password).click();
		driver.findElement(Login_Bena_Locators.resend_VerifyNo_Again).click();
	}
	
}
