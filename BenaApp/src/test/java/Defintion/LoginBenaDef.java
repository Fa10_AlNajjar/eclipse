package Defintion;

import com.experitest.auto.BaseTest;

import Action.LoginBena;
import io.cucumber.java.en.When;

public class LoginBenaDef extends BaseTest {
	@When("i login successfully")
	public void i_login_successfully() throws InterruptedException {
		LoginBena.Login();
	}

	

	@When("login passed")
	public void login_passed() throws InterruptedException {
		LoginBena.Login_by_phone();
	}



	@When("Login by invalid email")
	public void login_by_invalid_email() {
		LoginBena.Login_By_InvalidEmail();
	}



	@When("Login by wrong Password")
	public void login_by_wrong_Password() {
		LoginBena.Login_By_WrongPassword();
	}


	@When("Press Skip")
	public void press_Skip() throws InterruptedException {
		LoginBena.Skip();
	}



	@When("Go to Sign Up Screen")
	public void go_to_Sign_Up_Screen() {
		LoginBena.GoTo_SignUp_From_Login();
	}



	@When("forget password")
	public void forget_password() {
		LoginBena.forget_password();
	}

	

	@When("resend verify number")
	public void resend_verify_number() {
		LoginBena.resend_verify_number();
	}

	@When("close the App")
	public void close_the_App() {
	    driver.quit();
	}
}
