package Defintion;

import java.net.MalformedURLException;

import com.experitest.auto.BaseTest;

import Action.RegisterBena_Action;
import Locators.RegisterBena_Locators;
import io.appium.java_client.MobileElement;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegisterBena_Def extends BaseTest {
	@Given("Iam in Bena APP")
	public void iam_in_Bena_APP() throws MalformedURLException, InterruptedException {
      BaseTest.setup();
	}
	@Then("Sign up happy scenario")
	public void sign_up_happy_scenario() throws InterruptedException {
		RegisterBena_Action.Sign_Up();

	}
}
