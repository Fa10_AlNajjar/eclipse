package Defintion_Forefox;

import org.sikuli.script.FindFailed;

import Action_FireFox.Login_Action_FF;
import Zool.zool.BaseTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginFF extends BaseTest {
	@Given("Iam in zool website ff")
	public void iam_in_zool_website_ff() {
		BaseTest.openbrowserFF();
		BaseTest.navigateTo_ByFF(Zoolurl);
	}

	@Then("Close the FF")
	public void close_the_FF() {
		driver1.close();
	}

	@When("login using valid email FF")
	public void login_using_valid_email_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.LoginUsingValidEmail();
	}

	@When("login using Invalid email FF")
	public void login_using_Invalid_email_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.LoginUsingInvalidEmail();
	}

	@When("login via Facebook FF")
	public void login_via_Facebook_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.LoginViaFB();
	}

	@When("Press on submit button FF")
	public void press_on_submit_button_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.PressLoginButton();
	}

	@When("close dialouge box FF")
	public void close_dialouge_box_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.close_login_dialogue_box();
	}

	@When("forget my password FF")
	public void forget_my_password_FF() throws FindFailed, InterruptedException {
		Login_Action_FF.ForgetPassword();
	}

}
