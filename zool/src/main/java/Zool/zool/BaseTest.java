package Zool.zool;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.script.Screen;

public class BaseTest {
	protected static Screen screen = new Screen();
	public static WebDriver driver;
	public static WebDriver driver1;
	public static String Zoolurl = "https://zoolc.co.il";
	static Random rand = new Random();
	public static int rnd = rand.nextInt(999) + 1;

	public static void openbrowser() {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	public static void navigateTo(String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("navigate to : " + driver.getCurrentUrl());
		System.out.println(driver.getTitle());
	}

	public static void openbrowserFF() {
		System.setProperty("webdriver.gecko.driver", "./Driver/geckodriver.exe");
		driver1 = new FirefoxDriver();
		driver1.manage().window().maximize();
	}

	public static void navigateTo_ByFF(String url) {
		driver1.get(url);
		driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("navigate to : " + driver1.getCurrentUrl());
		System.out.println(driver1.getTitle());
	}
}
