package defintion;

import org.sikuli.script.FindFailed;

import Action.Cart_Action;
import Zool.zool.BaseTest;
import io.cucumber.java.en.When;

public class cart extends BaseTest {
	@When("Add product to my cart")
	public void add_product_to_my_cart() throws InterruptedException {
		Cart_Action.Add_Single_Product();
	}

	@When("Add multiple product to my cart")
	public void add_multiple_product_to_my_cart() throws InterruptedException {
		Cart_Action.Add_Multiple_Product();
	}

	@When("Delete the product")
	public void delete_the_product() throws FindFailed, InterruptedException {
		Cart_Action.Delet_from_Cart();
	}

	@When("update quantity")
	public void update_quantity() throws FindFailed, InterruptedException {
		Cart_Action.update_quantity();
	}
}
