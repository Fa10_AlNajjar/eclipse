package defintion;

import Action.Search_Action;
import Zool.zool.BaseTest;
import io.cucumber.java.en.When;

public class Search extends BaseTest {
	@When("Empty Search")
	public void empty_Search() throws InterruptedException {
		Search_Action.Empty_Search();
	}

	@When("Search")
	public void search() throws InterruptedException {
		Search_Action.Search_On_Product();
	}
}
