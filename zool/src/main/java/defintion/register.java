package defintion;

import org.sikuli.script.FindFailed;

import Action.Register_Action;
import Zool.zool.BaseTest;
import io.cucumber.java.en.When;

public class register extends BaseTest {
	@When("register using valid email")
	public void register_using_valid_email() throws InterruptedException, FindFailed {
		Register_Action.Valid_Register();
	}

	@When("Register uding invalid email")
	public void register_uding_invalid_email() throws InterruptedException, FindFailed {
		Register_Action.InValid_Register();
	}

	@When("Register via facebook")
	public void register_via_facebook() throws FindFailed, InterruptedException {
		Register_Action.Register_VIA_FB();
	}

	@When("logo out")
	public void logo_out() throws FindFailed, InterruptedException {
		Register_Action.Logout();
	}

}
