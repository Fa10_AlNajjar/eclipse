package defintion;

import Action.Pages_Action;
import Zool.zool.BaseTest;
import io.cucumber.java.en.When;

public class Pages extends BaseTest {

	@When("Iam in Home Page")
	public void iam_in_Home_Page() {

		Pages_Action.HomePage();
	}

	@When("Iam in About us page")
	public void iam_in_About_us_page() {
		Pages_Action.AboutUs();
	}

	@When("Iam in New product page")
	public void iam_in_New_product_page() {
		Pages_Action.New_Product_Page();
	}

	@When("Iam in Package-List Page")
	public void iam_in_Package_List_Page() {
		Pages_Action.Package_Page();
	}

	@When("Iam in Used-Product Page")
	public void iam_in_Used_Product_Page() {
		Pages_Action.Used_Product_Page();
	}

	@When("Iam in Contact us page")
	public void iam_in_Contact_us_page() {
		Pages_Action.contact_us_Page();
	}

}
