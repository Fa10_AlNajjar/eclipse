package defintion;

import org.sikuli.script.FindFailed;

import Action.Login_Action;
import Zool.zool.BaseTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Login extends BaseTest {
	@Given("Iam in zool website")
	public void iam_in_zool_website() {
		BaseTest.openbrowser();
		BaseTest.navigateTo(Zoolurl);
	}

	@When("login using valid email")
	public void login_using_valid_email() throws FindFailed, InterruptedException {
		Login_Action.LoginUsingValidEmail();
	}

	@When("login using Invalid email")
	public void login_using_Invalid_email() throws FindFailed, InterruptedException {
		Login_Action.LoginUsingInvalidEmail();
	}

	@When("login via Facebook")
	public void login_via_Facebook() throws FindFailed, InterruptedException {
		Login_Action.LoginViaFB();
	}

	@When("Press on submit button")
	public void press_on_submit_button() throws FindFailed, InterruptedException {
		Login_Action.PressLoginButton();
	}

	@When("close dialouge box")
	public void close_dialouge_box() throws FindFailed, InterruptedException {
		Login_Action.close_login_dialogue_box();
	}

	@When("forget my password")
	public void forget_my_password() throws FindFailed, InterruptedException {
		Login_Action.ForgetPassword();
	}

	@Then("Close the browser")
	public void close_the_browser() {
		driver.close();
	}

}
