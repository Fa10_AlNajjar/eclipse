package Action;

import org.openqa.selenium.By;

import Zool.zool.BaseTest;

public class Pages_Action extends BaseTest {
	public static void HomePage() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[1]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("https://zoolc.co.il/")) {
			System.out.println("Home Page Has Been Displayed");
		} else {
			System.out.println("Home Page Has Not Been Displayed ");
		}
	}

	public static void AboutUs() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[2]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("about-us")) {
			System.out.println("About-US Has Been Displayed");
		} else {
			System.out.println("About-Us Has Not Been Displayed ");
		}
	}

	public static void New_Product_Page() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[3]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("new-product-list")) {
			System.out.println("New Product Page Has Been Displayed");
		} else {
			System.out.println("New Product Page Has Not Been Displayed ");
		}
	}

	public static void Package_Page() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[4]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("packages-list")) {
			System.out.println("Package Page Has Been Displayed");
		} else {
			System.out.println("Package Page Has Not Been Displayed ");
		}
	}

	public static void Used_Product_Page() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[5]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("used-products-list")) {
			System.out.println("Used Product Page Has Been Displayed");
		} else {
			System.out.println("Used Product Page Has Not Been Displayed ");
		}
	}

	public static void contact_us_Page() {
		driver.findElement(By.xpath("//*[@id=\"navbarResponsive\"]/ul/li[6]/a")).click();
		String title = driver.getCurrentUrl();
		if (title.contains("contact-us")) {
			System.out.println("contact-us Page Has Been Displayed");
		} else {
			System.out.println("contact-us Page Has Not Been Displayed");
		}
	}
}
