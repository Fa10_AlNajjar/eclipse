package Action;

import org.openqa.selenium.By;

import Zool.zool.BaseTest;

public class Search_Action extends BaseTest {
	public static void Empty_Search() throws InterruptedException {

		driver.findElement(By.xpath("//*[@id=\"serachdiv\"]/div/form/div/button")).click();
		String title = driver.getCurrentUrl();
		Thread.sleep(3000);
		if (title.contains("&q=")) {
			System.out.println("Empty Search Has Been Passed");
		} else {
			System.out.println("Empty Search has been failed");
		}
	}

	public static void Search_On_Product() throws InterruptedException {

		driver.findElement(By.xpath("//*[@id=\"serachdiv\"]/div/form/div/input")).sendKeys("iphone");
		driver.findElement(By.xpath("//*[@id=\"serachdiv\"]/div/form/div/button")).click();
		String title = driver.getCurrentUrl();
		Thread.sleep(3500);
		System.out.println(title);
		if (title.contains("=iphone")) {
			System.out.println("Search About Product Has Been Passed");
		} else {
			System.out.println("Search has been failed");
		}
	}
}
