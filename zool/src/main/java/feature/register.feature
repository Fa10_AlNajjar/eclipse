#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file


  Scenario: Register using valid email
    Given Iam in zool website
    When register using valid email
    Then Close the browser 
    
    Scenario: Register using invalid email
     Given Iam in zool website
     When Register uding invalid email
     Then Close the browser
     
     Scenario: Register via facebook
      Given Iam in zool website
      When Register via facebook
      Then Close the browser
      
      Scenario: Logout 
       Given Iam in zool website
       When logo out 
       Then Close the browser 
    

 
