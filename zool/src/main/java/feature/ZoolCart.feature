#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Cart

  @tag1
  Scenario: Add product to my cart
    Given Iam in zool website
    When Add product to my cart
    Then Close the browser

  Scenario: Add multiple product to my cart
    Given Iam in zool website
    When Add multiple product to my cart
    Then Close the browser

  Scenario: Delete Product
    Given Iam in zool website
    When Delete the product
    Then Close the browser

  Scenario: Update quantity on my cart
    Given Iam in zool website
    When update quantity
    Then Close the browser
