package Action_FireFox;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import Zool.zool.BaseTest;

public class Login_Action_FF extends BaseTest {
	public static void LoginUsingValidEmail() throws FindFailed, InterruptedException {
		Thread.sleep(4000);
		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Pattern email = new Pattern("C:\\Users\\NadSoft\\eclipse-workspace\\zool\\ss\\email.PNG");
		screen.type(email, "faten@nadsoft.net");
		Pattern Password = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/password.PNG");
		screen.type(Password, "faten55132");
		Pattern Login1 = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login_btn.PNG");
		screen.click(Login1);
		Thread.sleep(2000);

	}

	public static void LoginUsingInvalidEmail() throws InterruptedException, FindFailed {

		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Pattern email = new Pattern("C:\\Users\\NadSoft\\eclipse-workspace\\zool\\ss\\email.PNG");
		screen.type(email, "faten@faten.com");
		Pattern Password = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/password.PNG");
		screen.type(Password, "faten55132");
		Pattern Login1 = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login_btn.PNG");
		screen.click(Login1);
		Thread.sleep(2000);
		String Message = driver.getPageSource();
		if (Message.contains("These credentials do not match our records.")) {
			System.out.println("These credentials do not match our records");
		} else {
			System.out.println("The Login has been done successfully");
		}

	}

	public static void LoginViaFB() throws InterruptedException, FindFailed {
		Thread.sleep(2500);
		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Pattern FB = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/FB.PNG");
		// screen.selectRegion();
		screen.capture(screen);
		screen.mouseMove(FB);
		screen.click(FB);
		Thread.sleep(3000);
		driver1.findElement(By.name("email")).sendKeys("testnadsoft@gmail.com");
		driver1.findElement(By.name("pass")).sendKeys("faten55132");
		Thread.sleep(2000);
		driver1.findElement(By.name("login")).click();
		System.out.println("Register Via FaceBook has been done successfully");
	}

	public static void PressLoginButton() throws InterruptedException, FindFailed {
		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Thread.sleep(2000);
		Pattern Login1 = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login_btn.PNG");
		screen.click(Login1);
		Thread.sleep(3500);
	}

	public static void close_login_dialogue_box() throws InterruptedException, FindFailed {
		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/button")).click();
		Thread.sleep(3000);
	}

	public static void ForgetPassword() throws FindFailed, InterruptedException {
		Pattern Login = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/login.PNG");
		screen.click(Login);
		Thread.sleep(1000);
		Pattern forget = new Pattern("C:/Users/NadSoft/eclipse-workspace/zool/ss/forget%20password.PNG");
		screen.click(forget);
		Thread.sleep(2000);
		driver.findElement(By.name("email")).sendKeys("faten@nadsoft.net");
		driver.findElement(By.xpath("/html/body/div/main/div/div/div/div/div[2]/form/div[2]/div/button")).click();
		Thread.sleep(1500);
		if (driver.getPageSource().contains("We have e-mailed your password reset link!"))

		{
			System.out.println("We have e-mailed your password reset link!");
		} else
			System.out.println("We have not e-mailed your password reset link!");

	}
}
