#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Navigate between pages

  Scenario: Home Page
    Given Iam in zool website
    When Iam in Home Page
    Then Close the browser

  Scenario: About us Page
    Given Iam in zool website
    When Iam in About us page
    Then Close the browser

  Scenario: New product page
    Given Iam in zool website
    When Iam in New product page
    Then Close the browser

  Scenario: Package-List Page
    Given Iam in zool website
    When Iam in Package-List Page
    Then Close the browser

  Scenario: Used-Product Page
    Given Iam in zool website
    When Iam in Used-Product Page
    Then Close the browser

  Scenario: Contact us page
    Given Iam in zool website
    When Iam in Contact us page
    Then Close the browser
