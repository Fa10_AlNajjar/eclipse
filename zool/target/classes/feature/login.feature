#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@UsingChromeBrowser
Feature: Login to zool

  Scenario: Login using valid email
    Given Iam in zool website
    When login using valid email
    Then Close the browser

  Scenario: Login using invalid email
    Given Iam in zool website
    When login using Invalid email
    Then Close the browser

  Scenario: Login via FB
    Given Iam in zool website
    When login via Facebook
    Then Close the browser

  Scenario: Press on submit button
    Given Iam in zool website
    When Press on submit button
    Then Close the browser

  Scenario: Close Dialoug Box
    Given Iam in zool website
    When close dialouge box
    Then Close the browser

  Scenario: Forget My Password
    Given Iam in zool website
    When forget my password
    Then Close the browser
