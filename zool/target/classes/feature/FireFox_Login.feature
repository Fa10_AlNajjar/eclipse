@UsingFireFox
Feature: Login using Firefox Browser

  Scenario: Login using valid email
    Given Iam in zool website ff
    When login using valid email FF
    Then Close the FF

  Scenario: Login using invalid email
    Given Iam in zool website ff
    When login using Invalid email FF
    Then Close the FF

  Scenario: Login via FB
    Given Iam in zool website ff
    When login via Facebook FF
    Then Close the FF

  Scenario: Press on submit button
    Given Iam in zool website ff
    When Press on submit button FF
    Then Close the FF

  Scenario: Close Dialoug Box
    Given Iam in zool website ff
    When close dialouge box FF
    Then Close the FF

  Scenario: Forget My Password
    Given Iam in zool website ff
    When forget my password FF
    Then Close the FF
